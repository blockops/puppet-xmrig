# @summary A short summary of the purpose of this defined type.
#
# A description of what this defined type does
#
# @example
#   xmrig::install { '6.9.0': download_url => 'https://github.com/xmrig/xmrig/releases/download/v6.9.0/xmrig-6.9.0-linux-x64.tar.gz' }
define xmrig::install_miner(
  String $version = $name,
  Stdlib::HTTPUrl $download_url = "https://github.com/xmrig/xmrig/releases/download/v${version}/xmrig-${version}-${::facts[kernel]}-x64.tar.gz",
  Optional[String] $checksum = $xmrig::version_table[$version],
  Optional[Stdlib::HTTPUrl] $checksum_url = "https://github.com/xmrig/xmrig/releases/download/v${version}/SHA256SUMS",
  Optional[String] $checksum_type = 'sha256',
  Stdlib::Absolutepath $install_path = $xmrig::install_path,
  Stdlib::Absolutepath $base_app_path = $xmrig::base_app_path,
  Stdlib::Absolutepath $binary_path = "${$install_path}/xmrig-${version}/xmrig",
) {
  $dirs = $install_path.split('/').reduce([]) |Array $acc, $value  | {
    $counter = $acc.length - 1
    unless empty($value) {
      $acc + "${$acc[$counter]}/${value}"
    } else {
      $acc
    }
  }

  @file{$dirs:
    ensure => directory
  }
  realize(File[$dirs])

  archive{"/tmp/${basename($download_url)}":
    ensure        => present,
    extract       => true,
    extract_path  => $install_path,
    source        => $download_url,
    checksum      => $checksum,
    checksum_type => $checksum_type,
    creates       => $binary_path,
    cleanup       => true,
    require       => [File[$dirs]]
  }
  -> file{$binary_path:
    ensure => file,
    mode   => '0555'
  }
}
