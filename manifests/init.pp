# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include xmrig
class xmrig(
  Hash $run_profiles,
  Hash $version_table,
  Stdlib::HttpUrl $base_download_url,
  String $latest_version = '6.12.0',
  String $default_version = $latest_version,
  Stdlib::Absolutepath $install_path = '/opt/miners',
  Stdlib::Absolutepath $base_app_path = '/opt/miners',
) {

  $wanted_versions = xmrig::get_versions($run_profiles, $default_version, $version_table, $base_download_url )
  $wanted_versions.each |$version, $data| {
    xmrig::install_miner{$version:
      install_path  => $install_path,
      base_app_path => $base_app_path,
      download_url  => $data[download_url],
      checksum      => $data[checksum]
    }
  }

  $run_profiles.each |String $name, Hash $data | {
    $service_name = $data.get(name, $name)
    $version = $data.get(version, $default_version)
    xmrig::service{$service_name:
      service_desc   => $data.get(service_desc, $service_name),
      start_options  => $data[start_options],
      binary_path    => "${$install_path}/xmrig-${$version}/xmrig",
      manage_service => $data.get(manage_service, true),
      enable_service => $data.get(enable_service, true),
      service_ensure => $data.get(service_ensure, running),
      require        => [Xmrig::Install_miner[$data[version]]]
    }
  }
  $allowed_files = $run_profiles.map |String $name, Hash $data | {
    $service_name = $data.get(name, $name)
    "${service_name}.service"
  }
  # purge any old files that are no longer being managed
  # does not stop old processes after switching versions
  file{'/root/stop_and_purge_xmrig.sh':
    ensure  => present,
    mode    => '0755',
    content => epp('xmrig/clean_systemd.sh.epp', {systemd_path => '/etc/systemd/system', allowed_files => $allowed_files}),
    notify  => Exec['stop and purge old xmrig files']
  }
  exec{'stop and purge old xmrig files':
    command     => 'bash /root/stop_and_purge_xmrig.sh',
    provider    => shell,
    refreshonly => true,
  }
}
