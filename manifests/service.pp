# @summary A short summary of the purpose of this defined type.
#
# A description of what this defined type does
#
define xmrig::service(
  Hash $start_options,
  String $binary_path = $xmrig::binary_path,
  String $service_name = $name,
  String $service_desc = $name,
  Boolean $manage_service = true,
  Enum[stopped, running] $service_ensure = running,
  Boolean $enable_service = true,
  String $running_user = 'root', # must be root to take advatage of certain things
  Enum[always, on-abort, on-failure] $restart = 'on-failure',
  String $after = 'network.target',
  String $stdout = "file:/var/log/${service_name}.log",
  String $stderr = 'inherit',
  String $target = 'multi-user.target'
) {

  $exec_start = "${binary_path} ${xmrig::generate_options($start_options)}"
  $unit_file = "xmrig-${service_name}.service"

  systemd::unit_file { $unit_file:
    content => epp('xmrig/xmrig.service.epp', {
      service_desc => $service_desc,
      exec_start   => $exec_start,
      service_name => $service_name,
      user         => $running_user,
      restart      => $restart,
      after        => $after,
      stdout       => $stdout,
      stderr       => $stderr,
      target       => $target,
    }),
  }

  if $manage_service {
    service {"xmrig-${service_name}":
      ensure    => $service_ensure,
      enable    => $enable_service,
      subscribe => Systemd::Unit_file[$unit_file],
    }
  }
}
