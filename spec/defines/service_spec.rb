# frozen_string_literal: true

require 'spec_helper'

describe 'xmrig::service' do
  on_supported_os.each do |os, _os_facts|
    context "on #{os}" do
      let(:title) { 'xmr' }
      let(:params) do
        {
          manage_service: false,
          binary_path: '/opt/miners/xmrig/xmrig',
          start_options: {
            '-a' => 'rx/0',
            '--tls' => '',
            '-o' => 'pool.hashvault.pro:5555',
            '-u' => 'vm52Efn893Y6cWwvYziFRPgP3y5RdjspKLJiaqLS7e3erJeaSKYgDvBKJAHsK4',
            '-p' => 'x',
            '--donate-level' => 1,
            '--http-enabled' => '',
            '--http-port' => 4030,
            '--rig-id' => '%{::facts.hostname}',
            '--no-color' => ''
          }
        }
      end

      it { is_expected.to compile }
    end
  end
end
