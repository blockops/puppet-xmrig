# frozen_string_literal: true

require 'spec_helper'

describe 'xmrig::install_miner' do
  let(:title) { 'namevar' }
  let(:params) do
    {
      install_path: '/opt/miners',
      base_app_path: '/opt/miners/xmrig',
      download_url: 'https://github.com/xmrig/xmrig/releases/download',
      checksum: 'ec46ae1a3511a923ed0c74b4f0975726705b6b672672b61314deee8ef7b75341'
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:title) { '6.8.1' }

      it { is_expected.to compile }
    end
  end
end
