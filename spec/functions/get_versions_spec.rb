# frozen_string_literal: true

require 'spec_helper'

describe 'xmrig::get_versions' do
  let(:args) do
    {
      'profiles' => {
        'monero' => {
          'desc' => 'Cpu miner for monero',
          'name' => 'monero-cpu',
          'version' => '6.10.0',
          'start_options' => {
            '--donate-level' => 1,
            '--http-enabled' => '',
               '--http-port' => 4030,
                '--no-color' => '',
                  '--rig-id' => 'f2a82001da5f',
                     '--tls' => '',
                        '-a' => 'rx/0',
                        '-o' => 'pool.hashvault.pro:5555:5555',
                        '-p' => 'x',
                        '-u' => '48Th9Lw2cnB1VYrZaot8UvYziFRPgP3y5RdjspKLJiaqLS7e3erJeaSKvm52Efn893Y6cWwYgDvBKJAHsK4F2jsW5WFBYNt'
          }
        }
      },
      'default_version' => '6.10.0',
      'version_table' => {
        '6.10.0' => '4079b3b34caa86dce0edc923a3292f5814dd555f28e8e6ec4c879a2c50a80787',
         '6.9.0' => '3dc6ca4c6c40d00850182616d2f7834b2b9d2dcda0619611c2612cead8d39a92'
      },
      'base_download_url' => 'https://github.com/xmrig/xmrig/releases/download'

    }
  end

  let(:output) do
    { '6.10.0' => {
      'url' => 'https://github.com/xmrig/xmrig/releases/download/v6.10.0/xmrig-6.10.0--x64.tar.gz',
       'checksum' => '4079b3b34caa86dce0edc923a3292f5814dd555f28e8e6ec4c879a2c50a80787'
    } }
  end

  it { is_expected.to run.with_params(args['profiles'], args['default_version'], args['version_table'], args['base_download_url']).and_return(output) }
end
