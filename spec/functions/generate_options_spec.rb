# frozen_string_literal: true

require 'spec_helper'

describe 'xmrig::generate_options' do
  let(:start_options) do
    {
      '--donate-level' => 1,
        '--http-enabled' => '',
           '--http-port' => 4030,
            '--no-color' => '',
              '--rig-id' => 'f2a82001da5f',
                 '--tls' => '',
                    '-a' => 'rx/0',
                    '-o' => 'pool.hashvault.pro:5555:5555',
                    '-p' => 'x',
                    '-u' => 'sW5WFBYNt'
    }
  end
  let(:out) do
    '--donate-level=1 --http-enabled --http-port=4030 --no-color --rig-id=f2a82001da5f --tls -a rx/0 -o pool.hashvault.pro:5555:5555 -p x -u sW5WFBYNt'
  end

  it { is_expected.to run.with_params(start_options).and_return(out) }
end
