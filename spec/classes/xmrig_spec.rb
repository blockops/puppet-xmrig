# frozen_string_literal: true

require 'spec_helper'

describe 'xmrig' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params)  do
        { default_version: '6.9.0' }
      end

      it { is_expected.to compile }
    end
  end
end
