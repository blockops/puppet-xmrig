function xmrig::generate_options(Hash $options) >> String {
    # if an option has no value it is a boolean switch
    # otherwise it is a flag and value
    $options.map | $key, $value | {
        if String($value).empty {
          $key
        } else {
          if $key =~ /\A\-{1,1}\w+\Z/ {
            "${key} ${value}"
          } else {
            "${key}=${value}"
          }
        }
    }.join(' ')
}
