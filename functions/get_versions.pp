# See https://puppet.com/docs/puppet/latest/lang_write_functions_in_puppet.html
# for more information on native puppet functions.
# @example
# {
#   "6.10.0" => {
#     "checksum" => "4079b3b34caa86dce0edc923a3292f5814dd555f28e8e6ec4c879a2c50a80787",
#          "url" => "https://github.com/xmrig/xmrig/releases/download/v6.10.0/xmrig-6.10.0-Linux-x64.tar.gz"
#   }
# }
function xmrig::get_versions(Hash $profiles, String $default_version, Hash $version_table, String $base_download_url) >> Hash {
    # gets an array of versions
    $versions = $profiles.map |String $name, Hash $profile| {
        $profile.get(version, $default_version)
    }.unique
    # injects a hash with version => and download information
    $versions.reduce({}) | Hash $data, String $version | {
        $version_checksum = $version_table[$version]
        $url = "${base_download_url}/v${version}/xmrig-${version}-${::facts[kernel]}-x64.tar.gz"
        $data + {$version => { url => $url, checksum => $version_checksum }}
    }
}
