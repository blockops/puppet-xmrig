
if $::os['family'] == 'RedHat' and $::os['release']['major'] == '8' {
  package{'epel-release': ensure => present }
}

class{'xmrig':
  run_profiles => {
    monero  => {
      name          => 'monero-cpu',
      desc          => 'Cpu miner for monero',
      version       => '6.10.0',
      start_options => {
        '-a'             => 'rx/0',
        '--tls'          => '',
        '-o'             => 'pool.hashvault.pro:5555',
        '-u'             => '48Th9Lw2cnB1VYrZaot8UvYziFRPgP3y5RdjspKLJiaqLS7e3erJeaSKvm52Efn893Y6cWwYgDvBKJAHsK4F2jsW5WFBYNt',
        '-p'             => x,
        '--donate-level' => 1,
        '--http-enabled' => '',
        '--http-port'    => 4030,
        '--rig-id'       => $::facts[hostname],
        '--no-color'     => '',
      }
    },
    xhv-gpu => {
      name          => 'xhv-gpu',
      desc          => 'GPU miner for XHV',
      start_options => {
        '-a'             => 'rx/0',
        '--tls'          => '',
        '-o'             => 'pool.hashvault.pro:5555',
        '-u'             => '48Th9Lw2cnB1VYrZaot8UvYziFRPgP3y5RdjspKLJiaqLS7e3erJeaSKvm52Efn893Y6cWwYgDvBKJAHsK4F2jsW5WFBYNt',
        '-p'             => x,
        '--donate-level' => 1,
        '--http-enabled' => '',
        '--http-port'    => 4031,
        '--rig-id'       => $::facts[hostname],
        '--no-color'     => '',
      }
    }
  }
}
